package nl.utwente.di.CelsiusExercise;

public class Translator {
    public int celsiusToFahrenheit(String celsius){
        return (Integer.parseInt(celsius)-32) * 5/9;
    }
}
